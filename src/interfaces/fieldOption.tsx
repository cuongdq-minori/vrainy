export const fieldOption = {
  many: {
    vni: "Mưa nhiều",
    color: "#ff3535",
    level: 1,
  },
  little: {
    vni: "Mưa ít",
    color: "#13f27b",
    level: 0.033,
  },
  alway: {
    vni: "Có mưa",
    color: "#f2d013",
    level: 0.08,
  },
};
