import { Unknown } from "./Unknown";

export interface City extends Unknown {
  name: string;
  rainyAround?: number;
  status: string;
  provinces?: Provinces[];
}
export interface Provinces extends Unknown {
  name: string;
  villageName: string;
  rainyAround?: number;
  status: string;
  area?: string;
}
