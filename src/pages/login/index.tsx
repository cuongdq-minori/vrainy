import React, { useLayoutEffect } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import Background from "../../components/atoms/Background";
import Header from "../../components/atoms/Header";
import Logo from "../../components/atoms/Logo";
import { FormInputLogin } from "../../components/molecules/FormInputLogin";
import { LoginProps } from "../../navigator";
import styles from "./style/index";

export const LoginAuthor = (props: LoginProps): JSX.Element => {
  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerShown: false,
      headerStatusBarHeight: 1,
    });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Background>
          <Logo />
          <Header>Welcome back.</Header>
          <FormInputLogin {...props} />
        </Background>
      </ScrollView>
    </SafeAreaView>
  );
};
