import { FirebaseAuthTypes } from "@react-native-firebase/auth";
import messaging from "@react-native-firebase/messaging";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { Alert, RefreshControl, SafeAreaView, ScrollView } from "react-native";
import { ListItem } from "../../components/organisms/ListItem";
import { City } from "../../interfaces/interface";
import { HomeProps } from "../../navigator";
import { DataSource } from "./data";
import styles from "./style/styles";

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export const Home = (props: HomeProps): JSX.Element => {
  const [isLoading, setLoading] = useState(false);
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);
  const [refreshing, setRefreshing] = React.useState(false);

  const onPressToListTown = (city: City) => {
    props.navigation.navigate("DistrictList", {
      item: city.provinces,
      headerName: city.name,
    });
  };

  useLayoutEffect(() => {
    props.navigation.setOptions({
      gestureEnabled: false,
    });
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    // auth().onAuthStateChanged((userState) => {
    //   setUser(userState);
    //   if (!user) props.navigation.navigate("Login");
    // });
    setLoading(true);
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      Alert.alert("A new FCM message arrived!", JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  return (
    <SafeAreaView style={styles.home}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ListItem
          DataSource={DataSource}
          onPress={onPressToListTown}
        ></ListItem>
      </ScrollView>
    </SafeAreaView>
  );
};
