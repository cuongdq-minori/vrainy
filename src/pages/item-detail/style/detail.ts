import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  detail: {
    backgroundColor: "#424c61",
    flex: 1,
  },

  mb10: {
    color: "white",
    fontSize: 20,
    marginTop: 10,
  },
  mb35: {
    marginBottom: 35,
  },
  container: {
    flex: 1,
  },
  paragraph: {
    color: "white",
  },
  //map content:
  mapConainer: {
    width: "97%",
    top: 10,
    marginBottom: 10,
    alignSelf: "center",
  },
  heatmap: {
    height: 300,
  },
  //card content:
  cardContainer: {
    top: 10,
    width: "97%",
    alignSelf: "center",
    marginBottom: 19,
  },
  leftContentCard: { width: "15%", justifyContent: "center" },
  arroundRainy: {
    fontSize: 30,
    alignSelf: "center",
  },
  textUnit: { color: "black", fontSize: 15, alignSelf: "center" },
  cardBody: { justifyContent: "center" },
  titleCardBody: { fontSize: 16 },
  //footer & button reset data
  executionText: {
    marginTop: 10,
    fontSize: 15,
    color: "white",
    alignSelf: "center",
  },
  btnResetData: {
    backgroundColor: "#959AA6",
    marginTop: 10,
    alignSelf: "center",
  },
});

export default styles;
