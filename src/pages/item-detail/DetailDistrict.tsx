import React, { useLayoutEffect } from "react";
import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { Button } from "react-native-paper";
import CardDetail from "../../components/molecules/CardDetail";
import { Map } from "../../components/molecules/Map";
import { DetailProps } from "../../navigator";
import styles from "./style/detail";
const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export const DetailCity = (props: DetailProps): JSX.Element => {
  const { headerName, detail } = props.route.params;
  const [refreshing, setRefreshing] = React.useState(false);

  useLayoutEffect(() => {
    props.navigation.setOptions({ headerTitle: headerName });
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <SafeAreaView style={styles.detail}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.mapConainer}>
          <Map></Map>
        </View>
        <View style={styles.cardContainer}>
          <CardDetail detail={detail} />
        </View>
        <View>
          <Text style={styles.executionText}>20/9/2019 - 20/11/2020</Text>
        </View>
        <View>
          <Button
            mode="contained"
            style={styles.btnResetData}
            onPress={onRefresh}
          >
            Cập nhật
          </Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
