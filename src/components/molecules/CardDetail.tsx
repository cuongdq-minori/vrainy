import React, { memo } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card, Paragraph, Title } from "react-native-paper";
import { fieldOption } from "../../interfaces/fieldOption";
import { Provinces } from "../../interfaces/interface";
import BadgeStatus from "../atoms/BadgeStatus";

interface CardDetailProps {
  detail: Provinces;
}

const LeftContent = ({ detail }: CardDetailProps): JSX.Element => {
  return (
    <View style={{ flexDirection: "column" }}>
      <Text
        style={[
          styles.arroundRainy,
          { color: fieldOption[detail.status].color },
        ]}
      >
        {detail.rainyAround}
      </Text>
      <Text style={styles.textUnit}>(mm)</Text>
    </View>
  );
};

const CardDetail = ({ detail }: CardDetailProps) => (
  <Card>
    <Card.Title
      title={detail.name}
      subtitle={
        <BadgeStatus
          statusRain={fieldOption[detail.status].vni}
          color={fieldOption[detail.status].color}
        ></BadgeStatus>
      }
      left={() => <LeftContent detail={detail} />}
      leftStyle={styles.leftContentCard}
    />
    <Card.Content style={styles.cardBody}>
      <Title style={styles.titleCardBody}>Phường/Xã:{detail.villageName}</Title>
      <Paragraph>Khu vực: {detail.area}</Paragraph>
    </Card.Content>
  </Card>
);
const styles = StyleSheet.create({
  leftContentCard: { width: "15%", justifyContent: "center" },
  cardBody: { justifyContent: "center" },
  titleCardBody: { fontSize: 16 },
  arroundRainy: {
    fontSize: 30,
    alignSelf: "center",
  },
  textUnit: { color: "black", fontSize: 15, alignSelf: "center" },
});
export default memo(CardDetail);
