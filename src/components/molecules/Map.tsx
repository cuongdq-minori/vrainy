import React, { useState } from "react";
import { StyleSheet } from "react-native";
import MapView, { Heatmap, PROVIDER_GOOGLE } from "react-native-maps";

export const Map = (): JSX.Element => {
  const [region, setRegion] = useState({
    latitude: 16.072949,
    longitude: 108.221388,
    latitudeDelta: 0,
    longitudeDelta: 0.02,
  });
  return (
    <MapView
      provider={PROVIDER_GOOGLE}
      mapType="terrain"
      region={region}
      showsUserLocation={true}
      showsTraffic={false}
      showsBuildings={false}
      onRegionChangeComplete={(region) => setRegion(region)}
    >
      <Heatmap
        points={[
          { latitude: 16.072949, longitude: 108.221388, weight: 5 },
          { latitude: 16.0752, longitude: 108.221272, weight: 10 },
          { latitude: 16.07286, longitude: 108.22227, weight: 23 },
          { latitude: 16.072052, longitude: 108.222332, weight: 40 },
          { latitude: 16.072949, longitude: 108.221388, weight: 12 },
          { latitude: 16.072114, longitude: 108.221978, weight: 70 },
        ]}
        opacity={1}
        style={styles.heatmap}
      />
    </MapView>
  );
};
const styles = StyleSheet.create({
  heatmap: {
    height: 300,
  },
});
