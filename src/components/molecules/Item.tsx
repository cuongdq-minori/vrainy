import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { List, ProgressBar } from "react-native-paper";
import { theme } from "../../core/theme";
import { fieldOption } from "../../interfaces/fieldOption";
import { Any } from "../../interfaces/Unknown";

interface ItemProps {
  data: Any;
  onPress: (value) => void;
}
export const Item = ({ data, onPress }: ItemProps): JSX.Element => {
  return (
    <List.Item
      onPress={() => onPress(data)}
      style={styles.listItem}
      title={data?.name}
      titleStyle={styles.listTitle}
      description={() => (
        <View>
          <ProgressBar
            progress={fieldOption[data.status].level}
            color={fieldOption[data.status].color}
            style={styles.listProgressbar}
          />
          <Text style={styles.listText}>{fieldOption[data.status].vni}</Text>
        </View>
      )}
      left={(props) => (
        <View style={styles.listLeftContent}>
          <List.Icon
            {...props}
            style={styles.listLeftIcon}
            icon="cloud"
            color={fieldOption[data.status].color}
          />
          <Text style={styles.listLeftText}>{data.rainyAround}mm</Text>
        </View>
      )}
      right={(props) => (
        <List.Icon
          {...props}
          style={styles.listRightIcon}
          icon="chevron-right"
          color="white"
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  label: {
    color: theme.colors.secondary,
  },
  button: {
    marginTop: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
  //list item
  listItem: {
    margin: 5,
    backgroundColor: "#6d7893",
    borderRadius: 5,
  },
  listTitle: { color: "#ffffff", fontSize: 20 },
  listProgressbar: { width: 30, marginTop: 10 },
  listText: { color: "white" },
  listLeftContent: {
    width: 80,
    marginLeft: 10,
    borderRightWidth: 1,
    borderColor: "#8991a3",
  },
  listLeftText: { marginTop: 1, color: "#ffffff" },
  listLeftIcon: {
    borderRightColor: "white",
    marginBottom: 0,
  },
  listRightIcon: { alignSelf: "center", marginRight: 0 },
});
