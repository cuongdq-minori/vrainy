import React from "react";
import { StyleSheet, View } from "react-native";
import { Item } from "../../components/molecules/Item";
import { Any } from "../../interfaces/Unknown";
interface ListItemProps {
  DataSource?: Any[];
  onPress: (value: Any) => void;
}
export const ListItem = ({
  DataSource,
  onPress,
}: ListItemProps): JSX.Element => {
  return (
    <View style={styles.listItem}>
      {DataSource?.map((city, i) => (
        <Item key={i} data={city} onPress={onPress}></Item>
      ))}
    </View>
  );
};
const styles = StyleSheet.create({
  listItem: { top: 10, marginBottom: 20 },
});
