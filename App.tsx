import React from "react";
import { AppContainer } from "./src/navigator/index";

export default function App() {
  return <AppContainer />;
}
